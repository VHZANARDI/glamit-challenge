# Glamit Challenge
Examen técnico para evaluar habilidades y aptitudes como desarrollador.

## Objetivo

Se requiere desarrollar una aplicación con un servicio de API REST, donde se pueda
realizar las operaciones de GET y POST. El propósito de este servicio, tiene por finalidad el
de proporcionar información de un catálogo de productos, al mismo tiempo que una lista de
categorías

El producto debe tener los siguientes atributos:
- Nombre
- Categoría
- Precio
- URL de la imagen
- SKU (número o código unívoco de referencia del producto)

Una categoría debe tener los siguientes atributos:
- Nombre
- Código único

Para la operación de tipo GET, la api deberá contar con dos endpoints. El primero de ellos,
deberá devolver la lista de todos los productos, al mismo tiempo que contar con paginado y
opcionalmente filtrar por SKU. Luego, el segundo endpoint deberá permitir obtener la lista
de categorías existentes.
Para la operación de tipo POST, la api deberá disponibilizar un endpoint que recibirá los
datos por payload para solicitar la creación de un producto. En caso de existir el mismo,
notificar mediante un mensaje descriptivo.
Ejemplo de referencia del payload
{
“SKU”: “abc123”,
etc...
}

## Especificaciones técnicas

- Al iniciar la aplicación por primera vez, se deben crear los datos de los productos,
  esta creación de datos no debe ser por medio de un archivo SQL.
- La API debe contar un middleware que verifique si el header que permite el acceso
  al sistema está presente
- Las tablas de categoría y de productos deben estar en schemas diferentes
- Los endpoints deben rechazar cualquier otra operación para los que no estén
  definidos

## Deploy local

La app esta programada con Java 1.8 y Maven 3.6
Descomprimir el zip o clonar el repositorio ubicado en https://gitlab.com/VHZANARDI/glamit-challenge.
Luego ejecutar:

```shell
mvn clean install -DskipTests
```

```shell
mvn spring-boot:run
```

## Aclaraciones

- Se agrego Swagger para tener una mejor documentación de los distintos endpoint y la posibilidad de probar
los mismo sin utilizar postman. Esta alojado en: http://localhost:8080/swagger-ui.html

- Para realizar la paginanción del endpoint "/product/all", se utilizo la clase Pageable y Page que nos permite 
implementar de una manera sencilla dicho requerimiento. Sumado la versatilidad que tiene JPA a la hora de
consultar registros de la base.

- Utilicé H2 como base de datos para facilitar el deploy local ya que es un demo, cambiar a mysql o 
otro motor de base de datos seria trivial.

- En el momento de ejecutar el desarrollo, se pre-cargar algunos datos en la base. Ver en la
clase: GlamitApplication.

- Se implemento la clase ServiceException para tener las excepciones ordenas y poder informar el error  
de manera clara.

- No se permite crear categorías con el mismo código único.

- Se implemento un interceptor para validar el header que permite el acceso al sistema está presente
(dicho header es solo de ejemplo, no es necesario crear un token para consumir los endpoint, solo enviarlo
con algún dato cargado).

- Para ver los datos guardados en la base, recomiendo utilizar la siguiente url http://localhost:8080/h2-console/
ya que nos tiene una buena interfaz, parecida a un cliente de MySql (Usuario: glamit, Contraseña: glamit).
