package com.challenge.glamit.repository;

import com.challenge.glamit.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    Category findByCode(String name);

    Boolean existsByCode(String code);
}
