package com.challenge.glamit.repository;

import com.challenge.glamit.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    Page<Product> findBySkuContaining(String sku, Pageable pageable);

    Boolean existsByName(String name);
}
