package com.challenge.glamit.restImpl;

import com.challenge.glamit.dto.CategoryDTO;
import com.challenge.glamit.entity.Category;
import com.challenge.glamit.rest.CategoryRest;
import com.challenge.glamit.service.CategoryService;
import com.challenge.glamit.service.exception.ServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(tags = "Category")
@RestController
@RequestMapping(path="/category")
public class CategoryRestImpl implements CategoryRest {

    @Autowired
    private CategoryService categoryService;

    @Override
    @RequestMapping(value="/create", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> create(
            @RequestHeader("authorization") String authorization,
            @RequestBody CategoryDTO categoryDTO
    ){

        try{
            this.categoryService.createCategory(categoryDTO);

            Map<String, Object> response = new HashMap<>();
            return new ResponseEntity<>(response, HttpStatus.CREATED);

        } catch (ServiceException e) {
            Map<String, Object> responseError = new HashMap<>();
            responseError.put("message", e.getMessage());
            return new ResponseEntity<>(responseError, e.getStatus());

        } catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @RequestMapping(value="/all", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> allCategories(
            @RequestHeader("authorization") String authorization
    ){
        try {
            List<Category> categories = this.categoryService.allCategories();

            Map<String, Object> response = new HashMap<>();
            response.put("categories", categories);
            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (ServiceException e) {
            Map<String, Object> responseError = new HashMap<>();
            responseError.put("message", e.getMessage());
            return new ResponseEntity<>(responseError, e.getStatus());

        } catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
