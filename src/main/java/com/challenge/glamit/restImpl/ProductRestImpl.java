package com.challenge.glamit.restImpl;

import com.challenge.glamit.dto.ProductDTO;
import com.challenge.glamit.entity.Product;
import com.challenge.glamit.rest.ProductRest;
import com.challenge.glamit.service.ProductService;
import com.challenge.glamit.service.exception.ServiceException;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;

import java.util.HashMap;
import java.util.Map;

@Api(tags = "Product")
@RestController
@RequestMapping(path="/product")
public class ProductRestImpl implements ProductRest {

    @Autowired
    private ProductService productService;

    @Override
    @RequestMapping(value="/all", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> allProduct(
            @RequestHeader("authorization") String authorization,
            @RequestParam(required = false) String sku,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size
    ){
        try{
            Page<Product> pageTuts = this.productService.allProduct(sku,page,size);

            Map<String, Object> response = new HashMap<>();
            response.put("products", pageTuts.getContent());
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (ServiceException e) {
            Map<String, Object> responseError = new HashMap<>();
            responseError.put("message", e.getMessage());
            return new ResponseEntity<>(responseError, e.getStatus());

        } catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @RequestMapping(value="/create", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> createProduct(
            @RequestHeader("authorization") String authorization,
            @RequestBody ProductDTO productDTO
    ){
        try{
            this.productService.createProduct(productDTO);

            Map<String, Object> response = new HashMap<>();
            return new ResponseEntity<>(response, HttpStatus.CREATED);

        } catch (ServiceException e) {
            Map<String, Object> responseError = new HashMap<>();
            responseError.put("message", e.getMessage());
            return new ResponseEntity<>(responseError, e.getStatus());

        } catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
