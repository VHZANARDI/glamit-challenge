package com.challenge.glamit;

import com.challenge.glamit.entity.Category;
import com.challenge.glamit.entity.Product;
import com.challenge.glamit.repository.CategoryRepository;
import com.challenge.glamit.repository.ProductRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GlamitApplication {

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private ProductRepository productRepository;

	public static void main(String[] args) {
		SpringApplication.run(GlamitApplication.class, args);
	}

	@Bean
	InitializingBean sendDatabase() {
		return () -> {

			Category cat1 = new Category("Zapatilla", "ABC123");
			Category cat2 = new Category("Remera", "ABC124");
			Category cat3 = new Category("Pantalon", "ABC125");

			categoryRepository.save(cat1);
			categoryRepository.save(cat2);
			categoryRepository.save(cat3);

			Product product1 = new Product("Adidas Predator Verde", 13456.4, "urlPhoto1", "AHHKQOAUJ111", cat1);
			Product product2 = new Product("Mistral Azul", 2000, "urlPhoto2", "AHHKQOAUJ112", cat2);
			Product product3 = new Product("Levis Blanco", 5000.5, "urlPhoto3", "AHHKQOAUJ113", cat3);
			Product product4 = new Product("Nike Total 90", 14000.10, "urlPhoto5", "AHHKQOAUJ114", cat1);

			productRepository.save(product1);
			productRepository.save(product2);
			productRepository.save(product3);
			productRepository.save(product4);

		};
	}
}


