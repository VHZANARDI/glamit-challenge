package com.challenge.glamit.rest;

import com.challenge.glamit.dto.CategoryDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.Map;

public interface CategoryRest {

    @ApiOperation(value = "Create a category")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Success", response = ResponseEntity.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<Map<String, Object>> create(
            @RequestHeader("authorization") String authorization,
            @RequestBody CategoryDTO categoryDTO
    );

    @ApiOperation(value = "Get all existing Categories")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = ResponseEntity.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<Map<String, Object>> allCategories(
            @RequestHeader("authorization") String authorization
    );
}
