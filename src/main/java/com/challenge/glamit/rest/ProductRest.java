package com.challenge.glamit.rest;

import com.challenge.glamit.dto.ProductDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

public interface ProductRest {

    @ApiOperation(value = "Get all existing Products")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = ResponseEntity.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @RequestMapping(value="/all", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> allProduct(
            @RequestHeader("authorization") String authorization,
            @RequestParam(required = false) String sku,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size
    );

    @ApiOperation(value = "Create a Product")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Success", response = ResponseEntity.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @RequestMapping(value="/create", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> createProduct(
            @RequestHeader("authorization") String authorization,
            @RequestBody ProductDTO productDTO
    );
}
