package com.challenge.glamit.serviceImpl;

import com.challenge.glamit.dto.ProductDTO;
import com.challenge.glamit.entity.Category;
import com.challenge.glamit.entity.Product;
import com.challenge.glamit.repository.CategoryRepository;
import com.challenge.glamit.repository.ProductRepository;
import com.challenge.glamit.service.ProductService;
import com.challenge.glamit.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Page<Product> allProduct(String sku, int page, int size) throws ServiceException {

        try {
            Pageable paging = PageRequest.of(page, size);

            Page<Product> pageTuts;
            if (sku == null) {
                pageTuts = this.productRepository.findAll(paging);
            } else {
                pageTuts = this.productRepository.findBySkuContaining(sku, paging);
            }

            return pageTuts;
        } catch (Exception e){
            throw new ServiceException(ServiceException.Type.INTERNAL_ERROR);
        }
    }

    @Override
    public void createProduct(ProductDTO productDTO) throws ServiceException {

        try{

            if(this.productRepository.existsByName( productDTO.getName())){
                throw new ServiceException(ServiceException.Type.EXIST_PRODUCT);
            }

            Category category = this.categoryRepository.findByCode(productDTO.getCategoryCode());
            if(category == null){
                throw new ServiceException(ServiceException.Type.CATEGORY_NOT_FOUND);
            }

            Product product = new Product();
            product.setName(productDTO.getName());
            product.setPrice(productDTO.getPrice());
            product.setSku(productDTO.getSku());
            product.setUrlPhoto(productDTO.getUrlPhoto());
            product.setCategory(category);

            this.productRepository.save(product);

        } catch (ServiceException e) {
            throw new ServiceException(e);

        } catch (Exception e){
            throw new ServiceException(ServiceException.Type.INTERNAL_ERROR);
        }
    }
}
