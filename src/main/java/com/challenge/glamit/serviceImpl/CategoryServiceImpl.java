package com.challenge.glamit.serviceImpl;

import com.challenge.glamit.dto.CategoryDTO;
import com.challenge.glamit.entity.Category;
import com.challenge.glamit.repository.CategoryRepository;
import com.challenge.glamit.service.CategoryService;
import com.challenge.glamit.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public void createCategory(CategoryDTO categoryDTO) throws ServiceException {

        try {

            if(this.categoryRepository.existsByCode(categoryDTO.getCode())){
                throw new ServiceException(ServiceException.Type.EXIST_CATEGORY);
            }

            Category category = new Category();
            category.setName(categoryDTO.getName());
            category.setCode(categoryDTO.getCode());

            this.categoryRepository.save(category);

        } catch (ServiceException e) {
            throw new ServiceException(e);

        } catch (Exception e){
            throw new ServiceException(ServiceException.Type.INTERNAL_ERROR);
        }
    }

    @Override
    public List<Category> allCategories() throws ServiceException {

        try{
            return this.categoryRepository.findAll();
        } catch (Exception e){
            throw new ServiceException(ServiceException.Type.INTERNAL_ERROR);
        }
    }
}
