package com.challenge.glamit.service;

import com.challenge.glamit.dto.ProductDTO;
import com.challenge.glamit.entity.Product;
import com.challenge.glamit.repository.CategoryRepository;
import com.challenge.glamit.repository.ProductRepository;
import com.challenge.glamit.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ProductService {

    Page<Product> allProduct(String sku, int page, int size) throws ServiceException;

    void createProduct(ProductDTO productDTO) throws ServiceException;
}
