package com.challenge.glamit.service.exception;

import org.springframework.http.HttpStatus;

public class ServiceException extends Exception{

    private final Type type;

    public ServiceException(ServiceException e) {
        super(e.getMessage());
        this.type = e.type;
    }

    public class Body {
        String code;
        String message;

        public Body(String code, String message) {
            this.code = code;
            this.message = message;
        }
    }

    public ServiceException(Type type) {
        super(type.getMessage());
        this.type = type;
    }

    public ServiceException(Type type, Throwable cause) {
        super(type.getMessage(), cause);
        this.type = type;
    }

    public HttpStatus getStatus() {
        return this.type.getStatus();
    }
    public Body getBody() { return new Body(this.type.code, this.type.message); }
    public static String getGeneric() {return "{\"code\": \"999\", \"message\": \"Generic Error\"}"; }

    public enum Type {

        INTERNAL_ERROR("E000", "INTERNAL ERROR",HttpStatus.INTERNAL_SERVER_ERROR),
        CATEGORY_NOT_FOUND("E001", "NO CATEGORY FOUND ACCORDING TO THE CODE",HttpStatus.BAD_REQUEST),
        EXIST_PRODUCT("E002", "THERE IS THE PRODUCT WITH THE INDICATED NAME",HttpStatus.BAD_REQUEST),
        EXIST_CATEGORY("E002", "THERE IS THE CATEGORY WITH THE INDICATED NAME",HttpStatus.BAD_REQUEST),
        TRANSACTION_ERROR("E010", "Error al realizar el recupero de la transacción.",HttpStatus.INTERNAL_SERVER_ERROR);

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public HttpStatus getStatus() {
            return status;
        }

        public void setStatus(HttpStatus status) {
            this.status = status;
        }

        private String code;
        private String message;
        private HttpStatus status;

        Type(String code, String message, HttpStatus status){
            this.code = code;
            this.message = message;
            this.status = status;
        }
    }
}
