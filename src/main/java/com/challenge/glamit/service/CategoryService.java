package com.challenge.glamit.service;

import com.challenge.glamit.dto.CategoryDTO;
import com.challenge.glamit.entity.Category;
import com.challenge.glamit.service.exception.ServiceException;

import java.util.List;

public interface CategoryService {

    void createCategory(CategoryDTO category) throws ServiceException;
    List<Category> allCategories() throws ServiceException;
}
